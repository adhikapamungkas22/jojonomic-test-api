package http

import (
	handler "jojonomic-test-api/domain/api"
	svcApi "jojonomic-test-api/domain/api/service"

	"github.com/gin-gonic/gin"
)

func NewHTTPHandler(r *gin.Engine, api svcApi.ApiServiceProcessor)  {

	handler := handler.ApiHandler{
		ApiService: api,
	}
	// list API from database render to HTML
	r.GET("/", handler.ListHandler)
	// create api
	r.POST("api/create", handler.CreateHandler)
	// list api with response oke
	r.GET("/endpoint-a/list", handler.ListAPIHandler)
}
