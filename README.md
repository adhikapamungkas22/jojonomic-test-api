if you only want to run code, you just have to execute the command `go run main.go`

1. Sample Curl Insert API to db
`curl --location 'http://localhost:8001/api/create' \
--header 'Content-Type: application/json' \
--data '{
    "name" : "list",
    "url" : "/endpoint-a/list",
    "method" : "GET",
    "response" : "{“status”: “ok”}"
}'`

2. Sample Response with status ok
`curl --location 'http://localhost:8001/endpoint-a/list'`

3. Url for list API interface use simple HTML 
`http://localhost:8001/`