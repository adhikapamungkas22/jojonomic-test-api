package mongo

import (
	"context"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// InitMongo return mongo db read instance
func InitMongo(ctx context.Context) *mongo.Database {

	// init read mongodb
	host := os.Getenv("MONGO_HOST")
	database := os.Getenv("MONGO_DATABASE")
	client, err := mongo.NewClient(options.Client().ApplyURI(host))
	if err != nil {
		panic(err)
	}
	if err := client.Connect(ctx); err != nil {
		panic(err)
	}

	return client.Database(database)
}

