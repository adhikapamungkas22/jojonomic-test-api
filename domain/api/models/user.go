package models

type API struct{
	Name         string  `json:"name"`
	URL          string  `json:"url"`
	Method       string  `json:"method"`
	Response     string  `json:"response"`
}


type Response struct{
	Status string `json:"status"`
}