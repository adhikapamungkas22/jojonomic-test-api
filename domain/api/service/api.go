package service

import (
	"context"
	"jojonomic-test-api/domain/api/models"
	repo "jojonomic-test-api/domain/api/repository"
)

// ApiServiceProcessor interface
type ApiServiceProcessor interface{
	CreateAPI(ctx context.Context, req models.API) error
	ListAPI(ctx context.Context) (*models.Response, error)
	List(ctx context.Context) ([]models.API, error)
}

// ApiService struct
type ApiService struct{
	apiRepo repo.ApiRepoProcessor
}

// NewAPIService func for init  Service 
func NewAPIService(ar repo.ApiRepoProcessor) ApiService {
	return ApiService{
		apiRepo: ar,
	}
}

// CreateAPI func for create API
func (f ApiService) CreateAPI(ctx context.Context, req models.API) error {
	err := f.apiRepo.Insert(ctx, req)
	if err != nil {
		return err
	}
	return nil
}

// ListAPI API sample response
func (f ApiService) ListAPI(ctx context.Context) (*models.Response, error) {
	return &models.Response{
		Status: "ok",
	}, nil
}

// List func service get API from database
func (f ApiService) List(ctx context.Context) ([]models.API, error) {
	res, err := f.apiRepo.List(ctx)
	if err != nil {
		return nil, err
	}
	// if return from db
	return res, nil
}
