package service

import (
	"context"
	"errors"
	"jojonomic-test-api/domain/api/models"
	"testing"
)

const (
	ExpectedResponse = "Expected resp to be %v but it was %v"
)

type fakeApiRepo struct {
	resList []models.API
	err     error
}

func (f fakeApiRepo) Insert(ctx context.Context, req models.API) error {
	return f.err
}
func (f fakeApiRepo) List(ctx context.Context) ([]models.API, error) {
	return f.resList, f.err
}

var (
	mockPostiveRepo = fakeApiRepo{
		err: nil,
	}

	err                   error = errors.New("error database")
	mockNegativeCreateAPI       = fakeApiRepo{
		err: err,
	}
)

func TestApiService_CreateAPI(t *testing.T) {
	ctx := context.Background()
	cases := []struct {
		name     string
		request  models.API
		fakeRepo fakeApiRepo
		expect   error
	}{
		{
			name:     "Positive case",
			fakeRepo: mockPostiveRepo,
			expect:   nil,
		},
		{
			name:     "Negative case",
			fakeRepo: mockNegativeCreateAPI,
			expect:   err,
		},
	}

	for _, c := range cases {
		svc := NewAPIService(c.fakeRepo)
		err := svc.CreateAPI(ctx, c.request)
		if err != c.expect {
			t.Errorf(ExpectedResponse, c.expect, err)
		}

	}
}

func TestApiService_ListAPI(t *testing.T) {
	ctx := context.Background()
	cases := []struct {
		name     string
		fakeRepo fakeApiRepo
		expect   error
	}{
		{
			name:     "Positive case",
			fakeRepo: mockPostiveRepo,
			expect:   nil,
		},
	}

	for _, c := range cases {
		svc := NewAPIService(c.fakeRepo)
		_, err := svc.ListAPI(ctx)
		if err != c.expect {
			t.Errorf(ExpectedResponse, c.expect, err)
		}
	}
}

func TestApiService_List(t *testing.T) {
	ctx := context.Background()
	cases := []struct {
		name     string
		fakeRepo fakeApiRepo
		expect   error
	}{
		{
			name:     "Positive case",
			fakeRepo: mockPostiveRepo,
			expect:   nil,
		},
		{
			name:     "Negative case",
			fakeRepo: mockNegativeCreateAPI,
			expect:   err,
		},
	}

	for _, c := range cases {
		svc := NewAPIService(c.fakeRepo)
		_, err := svc.List(ctx)
		if err != c.expect {
			t.Errorf(ExpectedResponse, c.expect, err)
		}
	}
}
