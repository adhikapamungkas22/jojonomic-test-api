package repository

import (
	"context"
	"jojonomic-test-api/domain/api/models"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// ApiRepoProcessor interface
type ApiRepoProcessor interface{
	Insert(ctx context.Context, payload models.API) error
	List(ctx context.Context) ([]models.API, error)
}

// ApiRepository struct
type ApiRepository struct{
	collection *mongo.Collection
}

// NewAPIRepository func for init repository
func NewAPIRepository(mongodb *mongo.Database) ApiRepository  {
	return ApiRepository{
		collection: mongodb.Collection("api"),
	}
}

// Insert func for insert API 
func (r ApiRepository) Insert(ctx context.Context, payload models.API) error {
	log.Println("repository/Create")
	_, err := r.collection.InsertOne(ctx, payload)
	if err != nil {
		log.Println("error insert api", err)
		return err
	}

	return nil
}

// List func for get list API from database
func (r ApiRepository) List(ctx context.Context) ([]models.API, error) {
	res := []models.API{}
	c, err := r.collection.Find(ctx, bson.M{})
	if err != nil {
		log.Println("error get list api", err)
		return nil, err
	}
	err = c.All(ctx, &res)
	if err != nil {
		log.Println("error get list api", err)
		return nil, err
	}

	return res , nil
}