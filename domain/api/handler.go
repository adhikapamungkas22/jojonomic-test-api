package api

import (
	"context"
	"jojonomic-test-api/domain/api/models"
	"jojonomic-test-api/domain/api/service"
	"net/http"

	"github.com/gin-gonic/gin"
)

// APIHandler intreface
type APIHandler interface{
	CreateHandler()
	ListAPIHandler()
}

// ApiHandler struct
type ApiHandler struct{
	ApiService service.ApiServiceProcessor
}

// CreateHandler func for create API
func (h ApiHandler) CreateHandler(c *gin.Context)  {
	ctx := context.Background()

	req := models.API{}
	err := c.BindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code"  : http.StatusBadRequest,
			"msgErr": err.Error(),
		})
		return
	}

	err = h.ApiService.CreateAPI(ctx, req)
	if err != nil {
		c.JSON(http.StatusBadGateway, gin.H{
			"code"  : http.StatusBadRequest,
			"msgErr": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code"  :  http.StatusOK,
		"msg":  "Success save",
	})
}

// ListAPIHandler sample response ok
func (h ApiHandler) ListAPIHandler(c *gin.Context)  {
	ctx := context.Background()
	res, err := h.ApiService.ListAPI(ctx)
	if err != nil {
		c.JSON(http.StatusBadGateway, gin.H{
			"code"  : http.StatusBadGateway,
			"msgErr": err.Error(),
		})
		return
	}
	
	c.JSON(http.StatusOK, res)
}

// ListHandler func for get all api from database
func (h ApiHandler) ListHandler(c *gin.Context)  {
	ctx := context.Background()
	res, err := h.ApiService.List(ctx)
	if err != nil {
		c.JSON(http.StatusBadGateway, gin.H{
			"code"  : http.StatusBadGateway,
			"msgErr": err.Error(),
		})
		return
	}
	c.HTML(http.StatusOK, "index.html", res)
}