package cmd

import (
	"context"
	"fmt"
	apiSvc "jojonomic-test-api/delivery/http"
	apiRepo "jojonomic-test-api/domain/api/repository"
	svc "jojonomic-test-api/domain/api/service"
	database "jojonomic-test-api/infrastructure/database/mongo"
	"os"

	"log"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func Execute()  {

	ctx := context.Background()

	router := gin.New()

	err := godotenv.Load()
	if err != nil {
		log.Fatalf("Error getting env, %v", err)
	}

	router.LoadHTMLGlob("templates/*") 

	mongoDb := database.InitMongo(ctx)
	apiRepo := apiRepo.NewAPIRepository(mongoDb)
	svc := svc.NewAPIService(apiRepo)

	apiSvc.NewHTTPHandler(router, svc)

	router.Static("/static", "./static")
	// set port
	if err := router.Run(fmt.Sprintf(":%s", os.Getenv("PORT"))); err != nil {
		log.Fatal(err)
	}
}